import requests
from bs4 import BeautifulSoup
import csv

# webpage where requerst is made
uf = 'urllist.csv'
with open(uf, 'r') as fd:
    url = fd.read().split('\n')

# get lenght of list
listLenght = len(url)
nr = 1
emptyFileCount = 0

for i in url:
    # mottifying filename
    nullLen = ""
    rmHash = i.split("?")
    splitted_data = rmHash[0].split("/")
    filename=""
    filename = splitted_data[5] + " " + splitted_data[6] + " " + splitted_data[7] + '.txt'
    # num of file
    print(str(nr) + ' of ' + str(listLenght) + ' filename: ' + filename)

    # scraping content 
    webContent = requests.get(i)
    soup = BeautifulSoup(webContent.text, 'html.parser')

    # looking for a class called "jsx-1311002414" and adds 2 new lines between paragraphs
    texts = soup.find('div', attrs={'class': 'jsx-1311002414'})
    text = texts.get_text(separator="\n").strip()
    if(len(text)<1):
        emptyFileCount = emptyFileCount + 1
        print('file ' + str(nr) + ' is empty')
        nullLen = 'NULL '
    # saves file with date and time
    f = open('scrapes/' + nullLen + filename, 'w')
    f.writelines(text)

    # add 1 to count
    nr = nr + 1

# report empty files
if(emptyFileCount == 1):
    print('\n' + str(emptyFileCount) + ' file is empty')
elif(emptyFileCount > 1):
    print('\n' + str(emptyFileCount) + ' files are empty')
else:
    print('\n OK: none of the files were empty')
