# PYTHON simple webscraper

## Installation

to use scraper python is required to install [python 3.9](https://www.python.org/downloads/) and install dependetsies: 

```bash
pip install BeautifulSoup4
```

## Usage
Run:
```bash
python webscrape.py
```
and program creates new text files in scrapes folder it is in.

## License
[MIT](https://choosealicense.com/licenses/mit/)
